Resource Server JWT
---

The resource server hosts the [HTTP resources](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/Identifying_resources_on_the_Web) 
in which can be a document a photo or something else, in our case it will be a REST API protected by OAuth2.

For creating resource server dependensies
 
## Dependencies

```xml
   <dependencies>
	<dependency>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-web</artifactId>
	</dependency>

	<dependency>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-security</artifactId>
	</dependency>
	<dependency>
		<groupId>org.springframework.security.oauth.boot</groupId>
		<artifactId>spring-security-oauth2-autoconfigure</artifactId>
		<version>2.1.2.RELEASE</version>
	</dependency>

	<dependency>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-configuration-processor</artifactId>
		<optional>true</optional>
	</dependency>

	<dependency>
		<groupId>commons-io</groupId>
		<artifactId>commons-io</artifactId>
		<version>2.6</version>
	</dependency>
</dependencies>
```

## Defining our protected API

The code bellow defines the endpoint `/me` which returns the `Principal` object and it requires the authenticated 
user to have the `ROLE_USER` to access. 

```java
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping("/me")
public class UserController {

    @GetMapping
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<Principal> get(final Principal principal) {
        return ResponseEntity.ok(principal);
    }

}
```

The `@PreAuthorize` annotation validates whether the user has the given role prior to execute the code, to make it work
it's necessary to enable the `prePost` annotations, to do so add the following class:

```java
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfiguration {

}
```

The important part here is the `@EnableGlobalMethodSecurity(prePostEnabled = true)` annotation, the `prePostEnabled` flag
is set to `false` by default.

## Resource Server Configuration

```applocation.yml
	server:
	  servlet:
	    context-path: /version_number
	  port: 0000
	datasource:
	    platform: postgres
	    driverClassName: org.postgresql.Driver
	    initialization-mode: always
	    url: jdbc:postgresql://localhost:5432/edoc_authorization
	    username: edoc_user
	    password: edoc_password	
	security:
	  jwt:
	    public-key: classpath:public.txt
	  oauth2:
	    resource:
	      token-info-uri: http://localhost:9000/oauth/check_token
	      user-info-uri:  http://localhost:9000/users/me
	      prefer-token-info: true
	      id: resourceId
	    client:
	      client-id: clientId
	      client-secret: secret
	logging:
	  level:
	    ROOT: info
	    org:
	      hibernate:
		SQL: DEBUG
	      type:
		descriptor:
		  sql:
		    BasicBinder: TRACE
	  file: edoc_hr.log
	  path: logs/
  
```

Now let's add the Spring's configuration for the resource server.

```java
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

    private static final String ROOT_PATTERN = "/version_number**";
    
    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        resources.resourceId("resourceId");
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(HttpMethod.GET, ROOT_PATTERN).access("#oauth2.hasScope('read')")
                .antMatchers(HttpMethod.POST, ROOT_PATTERN).access("#oauth2.hasScope('write')")
                .antMatchers(HttpMethod.PATCH, ROOT_PATTERN).access("#oauth2.hasScope('write')")
                .antMatchers(HttpMethod.PUT, ROOT_PATTERN).access("#oauth2.hasScope('write')")
                .antMatchers(HttpMethod.DELETE, ROOT_PATTERN).access("#oauth2.hasScope('write')");
    }

}
```



>Powered by [SIMPLEX](https://www.oauth.com/) JAVA TEAM.

