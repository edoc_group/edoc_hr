package uz.simplex.hr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeResourceServer {

    public static void main(String... args) {
        SpringApplication.run(EmployeeResourceServer.class, args);
    }

}
